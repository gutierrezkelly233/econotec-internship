CREATE OR REPLACE FUNCTION fn_abecedario(letra text) 
RETURNS TABLE (solucion text) AS 
$$
DECLARE
	resultado text;
	letra_ascii integer;
BEGIN
	RAISE NOTICE 'Dato ingresado ==> %', letra;
	IF(letra ~ '^([a-zA-Z])$')
		THEN
			RAISE NOTICE 'Caracter ingresado correctamente';
			letra_ascii := ascii(letra);
			RAISE NOTICE 'convertido a ascii==>%',letra_ascii;
			SELECT CASE WHEN letra_ascii = 65 THEN 'RESULTADO A|B'
				   		WHEN letra_ascii = 90 THEN 'RESULTADO Y|Z'
				   		WHEN letra_ascii = 97 THEN 'RESULTADO a|b'
				   		WHEN letra_ascii = 122 THEN 'RESULTADO y|z'
				   		ELSE
				   			'RESULTADO '||CHR(letra_ascii-1)||'|'||CHR(letra_ascii)||'|'||CHR(letra_ascii+1)
				   END
				   INTO resultado;
		ELSE
			RAISE NOTICE 'ATENCION: El dato ingresado solamente puede ser una letra mayúscula o minúscula';
	END IF;
	RETURN QUERY EXECUTE 'SELECT '''||resultado||'''';
END;
$$ 
LANGUAGE plpgsql;

SELECT fn_abecedario('m');
