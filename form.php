<html>
    <head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>
    <body>
   
     <h2  class="text-center">FORMULARIO</h2>
        <div class="container">
            <?php echo validation_errors();?>
            
        <?php echo form_open(''); ?> 

      <div class="form-inline">
          <?php
          echo form_label('Nombre','nombre');
            $input=array(
                'name'  => 'nombre',
                'value' => '',
                'type'=>'text',
                'class' => 'form-control input-lg'
            );
                echo form_input($input);
          ?>

<div class="form-inline">
          <?php
          echo form_label('Apellido Paterno','apellido');
            $input=array(
                'name'  => 'ApellidoPat',
                'value' => '',
                'type'=>'text',
                'class' => 'form-control input-lg'
            );
                echo form_input($input);
          ?>

<div class="form-inline">
          <?php
          echo form_label('Apellido Materno','apellido');
            $input=array(
                'name'  => 'ApellidoMat',
                'type'=>'text',
                'value' => '',
                'class' => 'form-control input-lg'
            );
                echo form_input($input);
          ?>

    </div>
    <div class="form-inline">
          <?php
          echo form_label('Numero de Celular','celular');
            $input=array(
                'name'  => 'celular',
                'type'=>'number',
                'value' => '',
                'class' => 'form-control input-lg'
            );
                echo form_input($input);
          ?>

    </div>
    <?php echo form_submit('mysubmit','ENVIAR',"class='btn btn-danger'"); ?>
      <?php echo form_close(); ?>  

      </div>
    </body>
</html>