<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Sistema Presentacion</title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <link rel="icon" href="<?php echo site_url();?>/favicon.ico" type="image/x-icon" />

        <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,400,600,700,800" rel="stylesheet">
        

        <link rel="stylesheet" href="<?php echo base_url();?>asset/template/plugins/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" href="<?php echo base_url();?>asset/template/dist/css/theme.min.css">

        <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>


    </head>

    <body>

        <div class="wrapper">
            <div class="page-wrap">
                    <div class="main-content">
                        <div class="container-fluid">
                            <h1> 
                                Sistema de presentacion 
                            </h1>    
                            
                            

                            <div class="card-body">
                                    <form class="forms-sample" action="<?php echo base_url()?>Principal/guardar_subjefe" method="post">
                                        <div class="row">

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="exampleInputName1">Nombre</label>
                                                    <input type="text" class="form-control" id="nombre"  name="nombre" placeholder="Nombre.." required>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="exampleInputName1">Apellido Paterno</label>
                                                    <input type="text" class="form-control" id="paterno" name="paterno"  placeholder="Apellido Paterno" required pattern="[A-Za-z-. ]+">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="exampleInputName1">Apellido Materno</label>
                                                    <input type="text" class="form-control" id="materno" name="materno"  placeholder="Apellido Materno" required pattern="[A-Za-z-. ]+">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="exampleSelectGender">Celular</label>
                                                    <input class="form-control"  type="number"  id="celular" name="celular" placeholder="# Celular" required>
                                                </div>
                                            </div>
                                        </div>
                                    
                                        <button  type="submit" value="enviar" class="btn btn-primary">Guardar Datos Personales</button>

                                    </form>
                                </div>


                        </div>

                     </div>
               
                <footer class="footer">
                    <div class="w-100 clearfix">
                        <span class="text-center text-sm-left d-md-inline-block">Copyright © 2022 v2.0. JhioSA.</span>
                        <span class="float-none float-sm-right mt-1 mt-sm-0 text-center">Crafted with <i class="fa fa-heart text-danger"></i> by <a class="text-dark" target="_blank">Presentacion</a></span>
                    </div>
                </footer>
            </div>
        </div>
        
        
        

        <div class="modal fade apps-modal" id="appsModal" tabindex="-1" role="dialog" aria-labelledby="appsModalLabel" aria-hidden="true" data-backdrop="false">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="ik ik-x-circle"></i></button>
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="quick-search">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-4 ml-auto mr-auto">
                                    <div class="input-wrap">
                                        <input type="text" id="quick-search" class="form-control" placeholder="Search<?php echo base_url();?>." />
                                        <i class="ik ik-search"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                 
                </div>
            </div>
        </div>
        

        <script src="<?php echo base_url();?>asset/template/js/datatables.js"></script>
        <script src="<?php echo base_url();?>asset/template/dist/js/theme.min.js"></script>


        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <script>window.jQuery || document.write('<script src="<?php echo base_url();?>asset/template/src/js/vendor/jquery-3.3.1.min.js"><\/script>')</script>
<script src="<?php echo base_url();?>asset/src/js/vendor/jquery-3.5.1.min.js"></script>
        
         <script src="<?php echo base_url()?>asset/template/js/sweetalert.min.js"></script>
        
    <script type="text/javascript">
        $(document).ready(function(){
            $('#lightgallery').lightGallery();
        });
        </script>

        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/estilotabla.css">

    </body>
</html>
